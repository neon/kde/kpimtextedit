Source: kpimtextedit
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               doxygen,
               kf6-extra-cmake-modules,
               kf6-kcodecs-dev,
               kf6-kconfigwidgets-dev,
               kf6-kcoreaddons-dev,
               kf6-kiconthemes-dev,
               kf6-kio-dev,
               kf6-ktextaddons-dev,
               kf6-ktexttemplate-dev,
               kf6-ktextwidgets-dev,
               kf6-kwidgetsaddons-dev,
               kf6-kxmlgui-dev,
               kf6-sonnet-dev,
               kf6-syntax-highlighting-dev,
               ninja-build,
               pkg-kde-tools-neon,
               qt6-base-dev,
               qt6-tools-dev,
Standards-Version: 4.6.2
Homepage: https://projects.kde.org/projects/kde/pim/kpimtextedit
Vcs-Browser: https://anonscm.debian.org/git/pkg-kde/applications/kpimtextedit.git
Vcs-Git: https://anonscm.debian.org/git/pkg-kde/applications/kpimtextedit.git

Package: kpim6-kpimtextedit
Architecture: any
Multi-Arch: same
X-Neon-MergedPackage: true
Depends: ${misc:Depends}, ${shlibs:Depends}
Provides: libkpim6pimtextedit6abi2,
Replaces: libkpim6pimtextedit6,
          libkpim6pimtextedit-data,
          libkpim6pimtextedit-plugins,
          libkpim6pimtextedittexttospeech6,
          libkpim6pimtextedittexttospeech-data,
          libkf5pimtextedit5,
          libkf5pimtextedit5abi2,
          libkf5pimtextedit-data,
          libkf5pimtextedit-plugins,
          libkf5pimtextedittexttospeech5,
          libkf5pimtextedittexttospeech-data,
Description: library that provides a textedit with PIM-specific features
 KPIMTextedit provides a textedit with PIM-specific features. It also provides
 so-called rich text builders which can convert the formatted text in the text
 edit to all kinds of markup, like HTML or BBCODE.
 .
 This package is part of the KDE Development Platform PIM libraries module.

Package: kpim6-kpimtextedit-dev
Section: libdevel
Architecture: any
X-Neon-MergedPackage: true
Depends: kf6-ktextaddons-dev,
         kf6-ktextwidgets-dev,
         kf6-syntax-highlighting-dev,
         kpim6-kpimtextedit (= ${binary:Version}),
         kf6-sonnet-dev,
         ${misc:Depends}
Replaces: libkpim6pimtextedit-dev, 
          libkpim6pimtextedittexttospeech-dev,
          libkf5pimtextedit-dev,
          libkf5pimtextedittexttospeech-dev,
Description: kpim6kpimtextedit - development files
 KPIMTextedit provides a textedit with PIM-specific features. It also provides
 so-called rich text builders which can convert the formatted text in the text
 edit to all kinds of markup, like HTML or BBCODE.
 .
 This package contains the development files.

#### transitionals below

Package: libkpim6pimtextedit6
Architecture: all
Depends: kpim6-kpimtextedit, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkpim6pimtextedit-data
Architecture: all
Depends: kpim6-kpimtextedit, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkpim6pimtextedit-dev
Architecture: all
Depends: kpim6-kpimtextedit-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkpim6pimtextedit-plugins
Architecture: all
Depends: kpim6-kpimtextedit, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkpim6pimtextedittexttospeech6
Architecture: all
Depends: kpim6-kpimtextedit, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkpim6pimtextedittexttospeech-data
Architecture: all
Depends: kpim6-kpimtextedit, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkpim6pimtextedittexttospeech-dev
Architecture: all
Depends: kpim6-kpimtextedit-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkpim6pimtextedit6abi2
Architecture: all
Depends: kpim6-kpimtextedit, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf5pimtextedit-data
Architecture: all
Depends: kpim6-kpimtextedit, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf5pimtextedit-dev
Architecture: all
Depends: kpim6-kpimtextedit-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf5pimtextedit-plugins
Architecture: all
Depends: kpim6-kpimtextedit, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf5pimtextedittexttospeech5
Architecture: all
Depends: kpim6-kpimtextedit, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf5pimtextedittexttospeech-data
Architecture: all
Depends: kpim6-kpimtextedit, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf5pimtextedittexttospeech-dev
Architecture: all
Depends: kpim6-kpimtextedit-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.
